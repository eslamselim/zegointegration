﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UsersManagement.Data.Migrations
{
    public partial class AddAdminstrator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'423b1fea-b891-433f-981d-ac6f801c5db7', N'admin@example.com', N'ADMIN@EXAMPLE.COM', N'admin@example.com', N'ADMIN@EXAMPLE.COM', 1, N'AQAAAAEAACcQAAAAEH1fquymNHZGDUjzmQJqDk0nFkjMG+KjIo5Q00zFi+GsPCGUZtsMFgNP9cz5+yH7ZA==', N'AY6XEKOPZHAN56FJT44DJV2S4BKKTCBQ', N'e04b7005-18d1-4ee4-9a54-feaf627c6d75', NULL, 0, 0, NULL, 1, 0)\r\n");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [dbo].[AspNetUsers] WHERE [Id] = '423b1fea-b891-433f-981d-ac6f801c5db7' ");
        }
    }
}

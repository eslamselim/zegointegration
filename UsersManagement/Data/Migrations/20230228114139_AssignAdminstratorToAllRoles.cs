﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UsersManagement.Data.Migrations
{
    public partial class AssignAdminstratorToAllRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) SELECT N'423b1fea-b891-433f-981d-ac6f801c5db7', Id FROM [dbo].[AspNetRoles]\r\n");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [dbo].[AspNetUserRoles] WHERE [UserId] = '423b1fea-b891-433f-981d-ac6f801c5db7'");
        }
    }
}

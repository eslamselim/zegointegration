﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UsersManagement.Enums;
using UsersManagement.Models.IdentityModels;
using UsersManagement.ViewModels.IdentityViewModels;

namespace UsersManagement.Controllers.IdentityControllers
{
    [Authorize(Roles = "Admin" )]
    public class RolesController : Controller
    {
        private readonly RoleManager<Role> _roleManager;

        public RolesController(RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {
            var roles = await _roleManager.Roles.ToListAsync();
           
            return View(roles);
        }


        public async Task<IActionResult> CreateRole()
        {

            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateRole(RoleFormViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if(await _roleManager.Roles.AnyAsync(r => r.NameAr == model.NameAr.Trim()))
            {
                ModelState.AddModelError("NameAr", "This Role Already Exists!!");
                return View(model);
            }

            if (await _roleManager.RoleExistsAsync(model.NameEn.Trim()))
            {
                ModelState.AddModelError("NameEn", "This Role Already Exists!!");
                return View(model);
            }

            await _roleManager.CreateAsync(new Role 
            {  
                Name = model.NameEn.Trim(),
                NameAr = model.NameAr.Trim() 
            });

            return RedirectToAction("Index", "Users");
        }
    }
}

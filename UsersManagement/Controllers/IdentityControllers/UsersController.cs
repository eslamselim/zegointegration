﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UsersManagement.Models.IdentityModels;
using UsersManagement.ViewModels.IdentityViewModels;

namespace UsersManagement.Controllers.IdentityControllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UsersController(UserManager<IdentityUser> userManager, RoleManager<Role> roleManager, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _httpContextAccessor = httpContextAccessor;
        }



        public async Task<IActionResult> Index()
        {
            var users = await _userManager.Users.ToListAsync();
            var roles = await _roleManager.Roles.ToListAsync();

            var model = new UsersAndRolesViewModel
            {
                Users = users,

                Roles = roles.Select(r => new RoleViewModel
                {
                    RoleID = r.Id,
                    NameAr = r.NameAr,
                    NameEn = r.Name,
                    IsActive = false
                }),

                UsersWithRoles = users.Select(u => new UserRolesViewModel
                {
                    UserID = u.Id,
                    Email = u.Email,
                    Roles = _userManager.GetRolesAsync(u).Result
                }).Where(u => u.Roles.Count() > 0).ToList(),

            };

            return View(model);
        }



        public IActionResult CreateUser()
        {
            
            return View();
        }



        [HttpPost]
        public async Task<IActionResult> CreateUser(AddUserViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = new IdentityUser
            {
                Email = model.Email,
                UserName = model.Email
            };

            await _userManager.CreateAsync(user, model.Password);

            return RedirectToAction("Index", "Users");
        }




        [HttpPost]
        public async Task<IActionResult> AddToRoles(ChangeRolesViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            var roles = await _userManager.GetRolesAsync(user);

            await _userManager.AddToRolesAsync(user, model.ActiveRoles);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ChangeRoles(ChangeRolesViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            var roles = await _userManager.GetRolesAsync(user);

            await _userManager.RemoveFromRolesAsync(user, roles);

            await _userManager.AddToRolesAsync(user, model.ActiveRoles);

            return Ok();
        }


        public async Task<IActionResult> VideoCall()
        {
           var user = await GetCurrentUser();
            return View(user);
        }

        private async Task<IdentityUser> GetCurrentUser()
        {
            //return _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(x => x.Type == "user_id")?.Value;
            var uname = HttpContext.User.Identity?.Name;
            var user = await _userManager.FindByEmailAsync(uname);
            return user;
        }

    }
}

﻿using Microsoft.AspNetCore.Identity;

namespace UsersManagement.Models.IdentityModels
{
    public class Role : IdentityRole
    {
        public string NameAr { get; set; }
    }
}

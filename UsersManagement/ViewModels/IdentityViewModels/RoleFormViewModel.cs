﻿using System.ComponentModel.DataAnnotations;

namespace UsersManagement.ViewModels.IdentityViewModels
{
    public class RoleFormViewModel
    {
        [Required, StringLength(20)]
        public string NameEn { get; set; }


        [Required, StringLength(20)]
        public string NameAr { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using UsersManagement.Models.IdentityModels;

namespace UsersManagement.ViewModels.IdentityViewModels
{
    public class UsersAndRolesViewModel
    {
        public IEnumerable<IdentityUser> Users { get; set; } = new List<IdentityUser>();
        public IEnumerable<RoleViewModel> Roles { get; set; } = new HashSet<RoleViewModel>();
        public List<UserRolesViewModel> UsersWithRoles { get; set; } = new List<UserRolesViewModel>();
    }
}

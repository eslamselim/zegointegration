﻿namespace UsersManagement.ViewModels.IdentityViewModels
{
    public class RoleViewModel
    {
        public string RoleID { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public bool IsActive { get; set; }
    }
}

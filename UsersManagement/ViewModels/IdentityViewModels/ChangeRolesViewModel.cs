﻿namespace UsersManagement.ViewModels.IdentityViewModels
{
    public class ChangeRolesViewModel
    {
        public string Email { get; set; }
        public IList<string> ActiveRoles { get; set; }
    }
}

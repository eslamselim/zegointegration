﻿namespace UsersManagement.ViewModels.IdentityViewModels
{
    public class UserRolesViewModel
    {
        public string UserID { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Roles { get; set; } = new List<string>();
    }
}
